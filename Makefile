#!/usr/bin/make -f
CC:=gcc

.PHONY: default
default: pagetabledump

pagetabledump: pagetabledump.c
	$(CC) -Wall -O6 -s -o $@ $<
