/* Thanks @CiroSantili for https://stackoverflow.com/a/45128487/1783163 ! */

#define _XOPEN_SOURCE 700
#include <dirent.h>
#include <errno.h>
#include <fcntl.h> /* open */
#include <stdint.h> /* uint64_t  */
#include <stdio.h> /* printf */
#include <stdlib.h> /* size_t */
#include <string.h>
#include <sys/types.h>
#include <unistd.h> /* pread, sysconf */

typedef struct {
  uint64_t pfn : 55;
  unsigned int soft_dirty : 1;
  unsigned int file_page : 1;
  unsigned int swapped : 1;
  unsigned int present : 1;
} PagemapEntry;

int PAGE_SIZE;

/* Parse the pagemap entry for the given virtual address.
 *
 * @param[out] entry      the parsed entry
 * @param[in]  pagemap_fd file descriptor to an open /proc/pid/pagemap file
 * @param[in]  vaddr      virtual address to get entry for
 * @return 0 for success, 1 for failure
 */
int pagemap_get_entry(PagemapEntry *entry, int pagemap_fd, uintptr_t vaddr) {
  size_t nread;
  ssize_t ret;
  uint64_t data;
  uintptr_t vpn;

  vpn = vaddr / PAGE_SIZE;
  nread = 0;
  while (nread < sizeof(data)) {
    ret = pread(pagemap_fd, ((uint8_t*)&data) + nread, sizeof(data) - nread, vpn * sizeof(data) + nread);
    nread += ret;
    if (ret <= 0) {
      return EXIT_FAILURE;
    }
  }
  entry->pfn = data & (((uint64_t)1 << 55) - 1);
  entry->soft_dirty = (data >> 55) & 1;
  entry->file_page = (data >> 61) & 1;
  entry->swapped = (data >> 62) & 1;
  entry->present = (data >> 63) & 1;
  return EXIT_SUCCESS;
}

int dump_virtual_range(pid_t pid, uint64_t start, uint64_t end, int pagemap_fd) {
  for (uint64_t vaddr = start; vaddr <= end; vaddr += PAGE_SIZE) {
    PagemapEntry entry;
    if (pagemap_get_entry(&entry, pagemap_fd, vaddr)) {
      return EXIT_FAILURE;
    } else {
      uint64_t paddr = entry.pfn * PAGE_SIZE + vaddr % PAGE_SIZE;
      printf ("%i 0x%016lx 0x%016lx\n", pid, vaddr, paddr);
    }
  }
  return EXIT_SUCCESS;
}

int dump_proc(pid_t pid) {
  char pagemap_path[BUFSIZ];
  char maps_path[BUFSIZ];
  snprintf (pagemap_path, sizeof(pagemap_path), "/proc/%ju/pagemap", (uintmax_t)pid);
  snprintf (maps_path, sizeof(maps_path), "/proc/%ju/maps", (uintmax_t)pid);

  FILE* maps_file = fopen(maps_path, "r");
  if (!maps_file) {
    printf ("can not open %s, errno: %i\n", maps_path, errno);
    return EXIT_FAILURE;
  }

  int pagemap_fd = open(pagemap_path, O_RDONLY);
  if (pagemap_fd < 0) {
    printf ("can not open %s, errno: %i\n", pagemap_path, errno);
    return EXIT_FAILURE;
  }

  char maps_line[BUFSIZ];
  for (;;) {
    if (fgets(maps_line, sizeof(maps_line), maps_file) != maps_line) {
      if (errno) {
        printf ("can not read %s, errno: %i\n", maps_path, errno);
        return EXIT_FAILURE;
      } else {
        break;
      }
    } else {
      uint64_t start, end;
      char* end_str = strchr(maps_line, '-');
      if (!end_str) {
        printf ("invalid maps line: '%s'\n", maps_line);
        return EXIT_FAILURE;
      }
      *end_str=0;
      end_str++;

      start = strtoull(maps_line, NULL, 16);

      char* end_endp = strchr(end_str, ' ');
      if (!end_endp) {
        printf ("invalid maps line: '%s'\n", maps_line);
        return EXIT_FAILURE;
      }
      *end_endp=0;
      end = strtoull(end_str, NULL, 16);

      if (dump_virtual_range(pid, start, end, pagemap_fd) != EXIT_SUCCESS) {
        return EXIT_FAILURE;
      }
    }
  }

  if (fclose(maps_file)) {
    printf ("can not fclose %s, errno: %i\n", maps_path, errno);
    return EXIT_FAILURE;
  }

  if (close(pagemap_fd) < 0) {
    printf ("can not close %s, errno: %i\n", pagemap_path, errno);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int str_is_int(char* str) {
  for (int a = 0; str[a]; a++) {
    if (str[a] < '0' || str[a] > '9') {
      return 0;
    }
  }
  return 1;
}

int main(int argc, char **argv) {
  PAGE_SIZE = sysconf(_SC_PAGE_SIZE);
  pid_t pid;

  if (argc > 1) {
    printf("No accepted arguments yet.\n");
    return EXIT_FAILURE;
  }
  DIR* proc_dir = opendir("/proc");
  if (!proc_dir) {
    printf ("can not open /proc, errno: %i\n", errno);
    return EXIT_FAILURE;
  }
  for (;;) {
    errno = 0;
    struct dirent* dir = readdir(proc_dir);
    if (!dir) {
      if (errno) {
        printf ("can not read /proc, errno: %i\n", errno);
        return EXIT_FAILURE;
      } else {
        break;
      }
    } else {
      if (!str_is_int(dir->d_name)) {
        continue;
      }
      int pid = atol(dir->d_name);
      if (dump_proc(pid) != EXIT_SUCCESS) {
        return EXIT_FAILURE;
      }
    }
  }
  
  if (closedir(proc_dir)) {
    printf ("closedir() fails, errno: %i\n", errno);
  }

  return EXIT_SUCCESS;
}
